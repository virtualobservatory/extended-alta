# extended-alta

A python library extending the alta python functionality to support obtaining large(ish) queries from ALTA.

## Installation
This package should be pip installable by typing
```
pip install git+https://git.astron.nl/astron-sdc/extended-alta
```
This will install the dependencies as well. The CI/CD tests this code on all major versions of python between 3.7 and 3.10.

## Usage
Many queries on ALTA will relate on several pages of results. The main goal of this library is to make it as easy as possible to obtain all the data from a query, by looping over all pages.

### authentication
The authentication using a user name and password can be done in two ways:
1. By setting the `ALTAUSER` (username) and `ALTAPASSWORD` (password) environment variables.
2. By specifying the optional `user` (username) and `password` (password) arguments to the class initialisation.

If neither are specified, an exception will be raised. If both are specified, the arguments to the class initialisation take precedence. All arguments to the class initialisation are optional, the default values are:
```
host: "https://alta.astron.nl/altapi"
user: None (i.e. use environment variable)
password: None (i.e. use environment variable)
verbose: False
```
Assuming we have set the credentials in the environment variables and want to access the default ALTA server without verbosity (i.e. all default parameters), we can initialises the class as:
```
from extended_alta import extendedALTA

extended_alta = extendedALTA()
```
Now we need to define the query and the end point to access. As an example, we will query all data products (from the `dataproduct-flat` end point)
for all data corresponding to target `3C147_31`:

```
endpoint = "observations-flat"
query = "target=3C147_31"
extended_alta.setup_GET_json_list_page(resource=endpoint, query=query)
```
The easiest way to obtain the query parameters is by clicking the `filter` button on the end point URL
(e.g. https://alta.astron.nl/altapi/observations-flat), filling in the form, querying and look at what is added
to the URL to select. Unset parameters can be omitted in the query. Also note that the `setup_GET_json_list_page`
can take an optional argument `startpage` that specifies the first page to return.

Now, the class will behave as an iterator of the pages of results. If for instance we want to print all task IDs
that correspond to our query we can do:
```
for results_page in extended_alta:
    for result_entry in results_page['results']:
       print(result_entry['runId'])
```

## License
The extension library is licensed under the terms of the Apache License version 2.0
