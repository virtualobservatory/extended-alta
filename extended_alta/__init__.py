# SPDX-License-Identifier: Apache-2.0

from alta_interface.alta_interface import ALTA
from os import environ
import requests


class ExtendedALTA(ALTA):
    """Class extending the ALTA classes for access to the Apertif Long-Term Archive."""

    def __init__(
        self,
        host="https://alta.astron.nl/altapi",
        user=None,
        password=None,
        verbose=False,
    ):
        """Initialisation of the extended ALTA classs

        Parameters
        ----------
        host : url, optional
            The ALTA URL, by default https://alta.astron.nl/altapi
        user : str or None, optional, bu default None
            user name for ALTA, if this is None will try to read the ALTAUSER environment variable, by default None
        password : str or None, optional, by default None
            password for ALTA, if this is None will try to read the ALTAPASSWORD environment variable, by default None
        verbose : bool, optional
            If true, run verbosely, by default False
        """
        self.next_page = None
        user_password_exception = "No {vartype} specified. Please either provide a {vartype} using the {varname} parameter of the class or by setting the {varenv} environment variable."

        if user is None:
            try:
                user = environ["ALTAUSER"]
            except KeyError:
                raise KeyError(
                    user_password_exception.format(
                        vartype="username", varname="user", varenv="ALTAUSER"
                    )
                )
        if password is None:
            try:
                password = environ["ALTAPASSWORD"]
            except KeyError:
                raise KeyError(
                    user_password_exception.format(
                        vartype="password", varname="password", varenv="ALTAPASSWORD"
                    )
                )
        super().__init__(host, user, password, verbose)

    def GET_json_from_url(self, url):
        """Use GET to obtain a JSON document from a URL.
        In many cases this is a helper function and people using
        the class may want to use the iterator.

        Parameters
        ----------
        url : str
            url to send GET request to

        Returns
        -------
        json
            JSON data returned from the url.
        """
        response = requests.request("GET", url, headers=self.header)
        json_response = response.json()
        return json_response

    def GET_json_from_query(self, resource, query):
        """use GET to obtain a JSON document from the ALTA REST API end point resource
        using query ident. In many cases this is a helper function and people using
        the class may want to use the iterator.

        Parameters
        ----------
        resource : str
            REST end point to query
        query : str
            query to apply on the end point

        Returns
        -------
        json
            json document containing the data from the query
        """
        url = self.host + resource + "/" + str(query) + "/"
        self.verbose_print(("url: " + url))
        return self.GET_json_from_url(url)

    def init_json_iterator(self, resource, query, startpage=1):
        """Setup the class to be able to return to the paginated ALTA resuts.

        Parameters
        ----------
        resource : str
            REST end point to query
        query : str
            query to apply on the end point
        startpage : int, optional
            First page to return, by default 1
        """
        self.next_page = (
            self.host + resource + "?" + str(query) + "&page=" + str(startpage)
        )

    def GET_json_from_next_page(self):
        """Get the next page from the end point/query combination the class is set up to
        return, and set the page to query next by reading the 'links' -> 'next' key from the page.
        Due to the way ALTA is set up, http URLs will be automatically translated to https URLs.

        Returns
        -------
        json
            JSON data returned from the url. None if the previous page was the last one.

        Raises
        ------
        KeyError
            If the next_page class variable is None, this will be raised.
            Most probably you did not run the setup_GET_json_list_page class method.
        """
        if self.next_page is None:
            raise KeyError("No page available. Did not init?")
        if self.next_page == "LAST":
            self.next_page = None
            return None
        json_response = self.GET_json_from_url(
            self.next_page.replace("http://", "https://")
        )
        self.next_page = json_response["links"]["next"]
        if self.next_page is None:
            self.next_page = "LAST"
        return json_response

    def __iter__(self):
        """Return an iterator that will return all pages of the configured class.

        Returns
        -------
        self
            The class itself, that can be accessed as an iterator.
        """
        return self

    def __next__(self):
        """Iterator function, return the next page of the result

        Returns
        -------
        json
            the json of the next page in the iterator.

        Raises
        ------
        StopIteration
            After the last page has been returned, the iterator is stopped.
        """
        value = self.GET_json_from_next_page()
        if value is None:
            raise StopIteration
        return value
