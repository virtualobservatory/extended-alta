# SPDX-License-Identifier: Apache-2.0

from unittest import TestCase
from unittest.mock import MagicMock, patch, call, Mock
import extended_alta
import json


class ExtendedALTATest(TestCase):
    #    def setUp(self):
    #        ALTApatcher = patch("extended_alta.extended_alta.requests")
    #        self.ALTAmocker = ALTApatcher.start()

    def get_response(self, url):
        reqresp = MagicMock()
        reqresp.text = json.dumps({"token": "ceci n'est pas un token."})
        reqresp.url = url
        return reqresp

    @patch("requests.request")
    def get_class(self, reqmock):
        """Function to create the class to test with. This will set the host, headers and verbosity
        to default values. Since the superclass is mocked away it doesn't really matter what the arguments are."""
        url = "testurl/"
        reqmock.return_value = self.get_response(url)
        extalt = extended_alta.ExtendedALTA(
            host=url, user="tuser", password="tpassword"
        )
        extalt.host = "testurl/"
        extalt.header = "testheader"
        extalt.verbose = False
        return extalt

    @patch("requests.request")
    def test_parent_vars(self, reqmock):
        """Test that init parameters are correctly forwarded to the parent class."""
        tuser = "testuser"
        tpw = "testpassword"
        turl = "testurl"
        testvb = False
        reqmock.return_value = self.get_response(turl)
        extended_alta.ExtendedALTA(host=turl, user=tuser, password=tpw, verbose=testvb)
        reqmock.assert_called_with(
            "POST",
            f"{turl}/obtain-auth-token/",
            data={"username": tuser, "password": tpw},
        )
        reqmock.reset_mock()
        extended_alta.environ = {"ALTAUSER": tuser, "ALTAPASSWORD": tpw}
        extended_alta.ExtendedALTA(host=turl, verbose=testvb)
        reqmock.assert_called_with(
            "POST",
            f"{turl}/obtain-auth-token/",
            data={"username": tuser, "password": tpw},
        )
        reqmock.reset_mock()
        extended_alta.environ = {"ALTAPASSWORD": tpw}
        extended_alta.ExtendedALTA(host=turl, user=tuser, verbose=testvb)
        reqmock.assert_called_with(
            "POST",
            f"{turl}/obtain-auth-token/",
            data={"username": tuser, "password": tpw},
        )
        reqmock.reset_mock()
        extended_alta.environ = {"ALTAUSER": tuser}
        extended_alta.ExtendedALTA(host=turl, password=tpw, verbose=testvb)
        reqmock.assert_called_with(
            "POST",
            f"{turl}/obtain-auth-token/",
            data={"username": tuser, "password": tpw},
        )
        reqmock.reset_mock()

    def test_aai_errors(self):
        """Test that not having credentials raises an exception."""
        try:
            del extended_alta.environ["ALTAUSER"]
            del extended_alta.environ["ALTAPASSWORD"]
        except KeyError:
            pass
        self.assertRaises(KeyError, extended_alta.ExtendedALTA, *("testurl",))
        extended_alta.environ = {"ALTAUSER": "testuser"}
        self.assertRaises(KeyError, extended_alta.ExtendedALTA, *("testurl",))
        extended_alta.environ = {"ALTAPASSWORD": "testpasword"}
        self.assertRaises(KeyError, extended_alta.ExtendedALTA, *("testurl",))

    @patch("extended_alta.requests.request")
    def test_GET_json_from_url(self, reqmock):
        """Test if getting the JSON calls the right request"""
        extalt = self.get_class()
        extalt.GET_json_from_url("testURL")
        reqmock.assert_has_calls(
            [call("GET", "testURL", headers="testheader"), call().json()]
        )

    @patch("extended_alta.ExtendedALTA.GET_json_from_url")
    def test_GET_json_from_query(self, get_json_mock):
        """Test that the GET_json_from_query calls the right URL"""
        extalt = self.get_class()
        extalt.GET_json_from_query(resource="Myendpoint", query="Myquery")
        get_json_mock.assert_called_with("testurl/Myendpoint/Myquery/")

    def test_init_json_iterator(self):
        """Test that the first page URL is correctly set"""
        extalt = self.get_class()
        host = extalt.host
        resource = "MyResource"
        query = "MyQuery"
        extalt.init_json_iterator(resource=resource, query=query)
        self.assertEqual(
            extalt.next_page, host + resource + "?" + str(query) + "&page=1"
        )
        extalt.init_json_iterator(resource=resource, query=query, startpage=10)
        self.assertEqual(
            extalt.next_page, host + resource + "?" + str(query) + "&page=10"
        )

    @patch("extended_alta.ExtendedALTA.GET_json_from_url")
    def test_GET_json_from_next_page(self, json_urlmock):
        """Test that the page is retrieved and the next URL is set"""
        extalt = self.get_class()
        extalt.next_page = "http://FirstPage"
        nexturl = "http://nextURL"
        rtval = {"links": {"next": nexturl}}
        json_urlmock.return_value = rtval
        self.assertEqual(extalt.GET_json_from_next_page(), rtval)
        self.assertEqual(extalt.next_page, nexturl)
        json_urlmock.assert_called_with("https://FirstPage")

    def test_GET_json_from_next_page_noinit(self):
        """Thest that uninitialised class will not return a page."""
        extalt = self.get_class()
        self.assertRaises(KeyError, extalt.GET_json_from_next_page)

    @patch("extended_alta.ExtendedALTA.GET_json_from_url")
    def test_iterator(self, get_json_mock):
        """Test if the iterator works as expected"""
        extalt = self.get_class()
        extalt.init_json_iterator("testResource", "testQuery")
        pagelist = [extalt.host + "testResource?testQuery&page=1"]
        pagelist.extend(["URLpag" + str(val) for val in range(5)])
        jsonlist = [
            {"somedata": "testdata", "links": {"next": page, "other": "previous"}}
            for page in pagelist[1:]
        ]
        get_json_mock.side_effect = jsonlist
        for ctr, value in enumerate(extalt):
            get_json_mock.assert_called_with(pagelist[ctr])
